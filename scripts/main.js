'use strict'

// Since another version of jQuery is used for the header
// from www.liberation.fr, jQuery here is not referred to
// as $ but as jq331

/* * * * * * * * * * * * * * * * * * * * * *
 *
 * Init app
 *
 * * * * * * * * * * * * * * * * * * * * * */

/* App state */
const state = {
  page: 'home',
  scroll: { home: 0 },
  swipe: {
    start: { posX: 0, posY: 0, time: 0 },
    end:   { posX: 0, posY: 0, time: 0 },
    diff:  { posX: 0, posY: 0, time: 0 },
    isActive: false
  }
}

/* Display home page on load */
activatePage('home')

/* Interactions */
jq331('.wrapper').on('click', e => {
  if (
    jq331(e.target).hasClass('wrapper') &&
    state.page !== 'home') {
    activatePage('home')
  }
})
jq331('.story-thumbnail').on('click', handleClickOnNavigation)
jq331('.articles__navigation-item').on('click', handleClickOnNavigation)
jq331('.article').on('click', handleClickOnArticle)
jq331('.article').on('touchstart', handleTouchStart)
jq331('.article').on('touchmove', handleTouchMove)
jq331('.articles__navigation').on('touchstart', handleTouchStart)
jq331('.articles__navigation').on('touchmove', handleTouchMove)
jq331('.articles__navigation-left-arrow').on('click', goToPrevArticle)
jq331('.articles__navigation-right-arrow').on('click', goToNextArticle)
jq331('.article-switch-story a').on('click', handleClickOnArticleSwitcher)
jq331('.article__close').on('click', () => activatePage('home'))
jq331('.share-article__facebook').on('click', facebookShare)
jq331('.share-article__twitter').on('click', twitterShare)
jq331('.share-article__print').on('click', printArticle)
jq331('.share-article__mail').on('click', sendArticleViaMail)

/* * * * * * * * * * * * * * * * * * * * * *
 *
 * Functions
 *
 * * * * * * * * * * * * * * * * * * * * * */

/* Handle click on stories thumbnails */
function handleClickOnNavigation (e) {
  e.preventDefault()
  const goTo = jq331(this).data('target-article')
  activateArticle(goTo)
}

/* Handle click on articles */
function handleClickOnArticle (e) {
  if (jq331(this).hasClass('active')) return
  if (
    jq331(e.target)
      .parent()
      .hasClass(
        'article-switch-story__content'
      )
  ) return
  e.preventDefault()
  const goTo = jq331(this).data('target-article')
  activateArticle(goTo)
}

/* Handle click on article links */
function handleClickOnArticleSwitcher (e) {
  e.preventDefault()
  const goTo = jq331(this).data('target-article')
  activateArticle(goTo)
}

/* Handle touch start on articles and articles nav */
function handleTouchStart (e) {
  const { swipe } = state
  const { now } = Date
  swipe.isActive = true
  swipe.start.time = now()
  swipe.start.posX = e.originalEvent
    .touches[0]
    .screenX
  swipe.start.posY = e.originalEvent
    .touches[0]
    .screenY
}

/* Handle touch move on articles and articles nav */
function handleTouchMove (e) {
  const { swipe } = state
  const { now } = Date
  const { abs } = Math
  swipe.end.time = now()
  swipe.end.posX = e.originalEvent
    .touches[0]
    .screenX
  swipe.end.posY = e.originalEvent
    .touches[0]
    .screenY
  swipe.diff.posX = swipe.end.posX - swipe.start.posX
  swipe.diff.posY = swipe.end.posY - swipe.start.posY
  swipe.diff.time = swipe.end.time - swipe.start.time
  if (
    abs(swipe.diff.posX) > 30 &&
    abs(swipe.diff.posY) < abs(swipe.diff.posX / 2) &&
    swipe.diff.time < 1000 &&
    swipe.isActive) {
    swipe.isActive = false
    let goTo = null
    if (swipe.diff.posX > 0) goTo = state.article - 1
    if (swipe.diff.posX < 0) goTo = state.article + 1
    if (goTo >= 1 && goTo <= 3) activateArticle(goTo)
  }
}

/* Activate previous article */
function goToPrevArticle (e) {
  e.preventDefault()
  const goTo = state.article > 1 ? state.article - 1 : 3
  activateArticle(goTo)
}

/* Activate next article */
function goToNextArticle (e) {
  e.preventDefault()
  const goTo = state.article < 3 ? state.article + 1 : 1
  activateArticle(goTo)
}

/* Activate an article */
function activateArticle (target = 1) {
  if (
    state.page === 'articles' &&
    state.article === target) return
  if (target < 1 || target > 3) return activatePage('home')
  if (state.page !== 'articles') activatePage('articles')
  jq331('.articles__navigation-item.active').removeClass('active')
  jq331('.article.active').removeClass('active')
  jq331('.articles__navigation-item').eq(target - 1).addClass('active')
  jq331('.article').eq(target - 1).addClass('active')
  switch (target) {
    case 2:
      jq331('.article').eq(0).css('z-index', 1)
      jq331('.article').eq(1).css('z-index', 2)
      jq331('.article').eq(2).css('z-index', 0)
      jq331('.articles__navigation-spacer').css('margin-left', '-100%')
      break
    case 3:
      jq331('.article').eq(0).css('z-index', 0)
      jq331('.article').eq(1).css('z-index', 1)
      jq331('.article').eq(2).css('z-index', 2)
      jq331('.articles__navigation-spacer').css('margin-left', '-200%')
      break
    default:
      jq331('.article').eq(0).css('z-index', 2)
      jq331('.article').eq(1).css('z-index', 1)
      jq331('.article').eq(2).css('z-index', 0)
      jq331('.articles__navigation-spacer').css('margin-left', 0)
  }
  state.article = target
}

/* Activate a page */
function activatePage (page = 'home') {
  if (state.page === 'home') {
    state.scroll.home = window.scrollY
  }
  jq331('#wrapper > *').hide()
  switch (page) {
    case 'articles':
      state.page = 'articles'
      jq331('#articles').show()
      break
    default:
      state.page = 'home'
      jq331('#home').show()
      window.scrollTo(0, state.scroll.home)
  }
}

/* Facebook share */
function facebookShare () {
  const articleUrl = jq331('meta[property="og:url"]').attr('content')
  const facebookUrl = 'http://www.facebook.com/sharer/sharer.php?u=' + articleUrl
  const features = 'width=575,height=400,menubar=no,toolbar=no'
  window.open(facebookUrl, '', features)
}

/* Twitter share */
function twitterShare () {
  const features = 'width=575,height=400,menubar=no,toolbar=no'
  const txt = jq331('meta[name="custom:tweet-text"]').attr('content')
  const url = jq331('meta[name="twitter:url"]').attr('content')
  const via = jq331('meta[name="custom:tweet-via"]').attr('content')
  const tweet = `${txt} ${url} via ${via}`
  const twitterUrl = `https://twitter.com/intent/tweet?original_referer=&text=${tweet}`
  window.open(twitterUrl, '', features)
}

/* Print article */
function printArticle () {
  window.print()
}

/* Send article via mail */
function sendArticleViaMail (e) {
  if (jq331(e.target).prop('tagName') === 'A') return
  const articleTitle = jq331('meta[property="og:title"').attr('content')
  const articleDescription = jq331('meta[property="og:description"]').attr('content')
  const articleUrl = jq331('meta[property="og:url"]').attr('content')
  const mailSubject = 'Lu sur Libération.fr'
  const mailBody = articleTitle +
    '%0D%0A%0D%0A' +
    articleDescription +
    '%0D%0A%0D%0A' +
    'Retrouvez cet article sur le site de Libération :' +
    '%0D%0A' +
    articleUrl
  const href = `mailto:?subject=${mailSubject}&body=${mailBody}`
  jq331('<a>click</a>')
    .attr('href', href)
    .appendTo(this)
  this.querySelector('a').click()
  jq331(this).find('a').remove()
}
